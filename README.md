# Statamic and foundation starter kit #

This is a starter template for [Statamic](http://statamic.com/) using the [foundation 4 theme](https://github.com/statamicthemes/foundation-4). Remember to get a [Statamic](http://statamic.com/) license!