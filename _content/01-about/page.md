---
title: About
_template: page
_fieldset: page
---
ANDY: You think you'll ever get out of here?

RED: Me? Yeah. One day when I got a long, white beard and two or three marbles rolling around upstairs. They'll let me out.

ANDY: Tell you where I'd go. Zihuatanejo.

RED: Zihua...?

ANDY: Zihuatanejo. It's in Mexico. A little place on the Pacific Ocean. You know what the Mexicans say about the Pacific? They say it has no memory. That's where I want to live the rest of my life. A warm place with no memory. Open up a little hotel right on the beach. Buy some worthless old boat and fix it up new. Take my guests out charter fishing. 

[...] 

**I guess it comes down to a simple choice, really. Get busy living... or get busy dying.**